const data = require('./data.json');

var o = {};

const reduce = function (r, e) {
    var key = e.id

    if (!o[key]) {
        o[key] = e
        r.push(o[key])
    } else {
        o[key].total += e.total
    }

    return r
}

var myData = [].concat.apply([], data.carts.map((cart) => {
    var id = cart.id;
    
    if (cart.items.length == 0) {
        return {
            id: id,
            total: 0
        }
    }

    return cart.items.map((item) => {
        return {
            id: id,
            total: getArticleValue(item.article_id) * item.quantity
        }
    })
})).reduce(reduce, [])

function getArticleValue(id) {
    var price = data.articles.filter((article) => {
        if (article.id === id) {
            return article.price;
        }
    }) 
    return price[0].price;
}

result = {
    carts: myData
}

console.log(result);
return result;

// Obs.: I'm not sure if I have to return a json file or an object with the result