const data = require('./data.json');

var o = {};

const reduce = function (r, e) {
    var key = e.id

    if (!o[key]) {
        o[key] = e
        r.push(o[key])
    } else {
        o[key].total += e.total
    }

    return r
}

var myData = [].concat.apply([], data.carts.map((cart) => {
    var id = cart.id;
    
    if (cart.items.length == 0) {
        return {
            id: id,
            total: getDeliveryPrice(null)
        }
    }

    return cart.items.map((item) => {
        return {
            id: id,
            total: getDeliveryPrice(item.article_id)
        }
        total_delivery = 0;
    })
})).reduce(reduce, [])

function getDeliveryPrice(id) {
    var item_price = 0;
    
    if (id) {
        var item_price = getArticleValue(id);
    }

    var delivery_price = data.delivery_fees.find((elem) => {
        return item_price >= elem.eligible_transaction_volume.min_price && item_price < elem.eligible_transaction_volume.max_price;
    })    
    
    console.log(delivery_price.price);
    return delivery_price.price;
}

function getArticleValue(id) {
    var price = data.articles.filter((article) => {
        if (article.id === id) {
            return article.price;
        }
    }) 
    return price[0].price;
}

result = {
    carts: myData
}

console.log(result);
return result;

// Obs.: I'm not sure if I have to return a json file or an object with the result